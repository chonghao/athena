/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "src/TrigCostSvc.h"
#include "TrigCostMonitor/TrigCostAuditor.h"
#include "TrigCostMonitor/EnhancedBiasWeightCompAlg.h"

DECLARE_COMPONENT( TrigCostSvc )
DECLARE_COMPONENT( TrigCostAuditor )
DECLARE_COMPONENT( EnhancedBiasWeightCompAlg )
