/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "../TrigL1FexJetMonitorTool_jFexSR.h"
#include "../TrigL1FexJetMonitorTool_jFexLR.h"
#include "../TrigL1FexJetMonitorTool_gFex.h"
#include "../TrigL1FexJetMonitorTool_jetRoI.h"
#include "../TrigL1FexJetMonitorAlgorithm.h"

DECLARE_COMPONENT(TrigL1FexJetMonitorAlgorithm)
DECLARE_COMPONENT(TrigL1FexJetMonitorTool_jFexSR)
DECLARE_COMPONENT(TrigL1FexJetMonitorTool_jFexLR)
DECLARE_COMPONENT(TrigL1FexJetMonitorTool_gFex)
DECLARE_COMPONENT(TrigL1FexJetMonitorTool_jetRoI)



